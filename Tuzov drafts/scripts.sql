SELECT
#         count(DISTINCT o.user_id),
#         count(DISTINCT o.id),
#         count(DISTINCT iso.id),
#     count(DISTINCT ps.id),
#     count(DISTINCT au.id),
    o.id                    AS order_id,
    o.user_id               AS user_id,
    o.date                  AS order_date,
    o.date_end              AS order_fulfilment_date,
    o.status                AS order_status,
    o.summ                  AS order_amount,
    o.metro                 AS order_metro,
    o.address               AS order_address,
    o.user_want_user_bonus  AS order_paid_by_bonuses,
    o.first_order           AS order_is_first,
    o.week_end              AS order_week,
    o.month_end             AS order_month,
    o.quarter_end           AS order_quarter,
    o.year_end              AS order_year,
    o.date_delivered        AS order_delivered_date,
    o.time_delivered        AS order_delivered_time,
    o.courier_id            AS order_courier,
    o.gift_sum              AS order_gift_amount_used,
    iso.itemset_id          AS item_set_id,
    iso.quantity            AS item_quantity,
    iso.summ                AS item_amount,
    iso.giftbox_id          AS item_giftbox_id,
    ps.title                AS set_title,
    ps.old_price            AS set_competitors_price,
    ps.price                AS set_price,
    ps.small_description    AS set_short_desc,
    ps.description          AS set_longest_desc,
    ps.active               AS set_is_active1,
    ps.description_from_set AS set_longer_desc,
    ps.date                 AS set_start_of_sales,
    ps.canshop              AS set_is_active2,
    ps.count_left           AS set_remainder,
    ps.kind                 AS set_kind,
    ps.related_description  AS set_description_for_upsell,
    ps.drink_back           AS set_drink_back,
    ps.successor_id         AS set_successor,
    ps.count_in_stock       AS set_stock,
    au.date_joined          AS user_invite_requested,
    au.date_activate        AS user_activated,
    au.last_activiti        AS user_last_activity,
    au.gender               AS user_gender
FROM
    core_order AS o,
    core_itemset_order AS iso,
    core_partofset AS ps,
    auth_user au
WHERE
    o.id = iso.order_id AND
    ps.id = iso.itemset_id AND
    au.id = o.user_id AND
    #     o.id = 1667 AND
    0 = 0
ORDER BY
    o.id;

SELECT *
FROM core_partofset
WHERE announce IS NOT NULL

SELECT *
FROM core_order
WHERE id = 1662

9314	23772
9313	23763	39746

9236    23607
9234    23119    38266

SELECT *
FROM core_order o
WHERE
    status = 3 AND
    NOT exists(
        SELECT *
        FROM core_itemset_order
        WHERE
            order_id = o.id
    )

SELECT *
FROM core_order o, (
                       SELECT
                           order_id,
                           sum(summ) AS summ
                       FROM core_itemset_order
                       GROUP BY order_id
                   ) iso
WHERE
    status = 3 AND
    o.id = iso.order_id AND
    o.summ > iso.summ + 10

SELECT *
FROM auth_user
WHERE last_name LIKE 'Tuz%'

SELECT *
FROM core_order
WHERE user_id = 29067

SELECT *
FROM core_itemset_order
WHERE order_id = 19298

select count(distinct user_id), count(id), sum(summ) from core_order where status=3 and date(date) between "2016-07-22" and "2016-07-28" and user_id in (select user_id from invisible2_logs.mail_logs_showcaselog where date(created) between "2016-07-22" and "2016-07-28" and user_list in (5) and sets_list=5);