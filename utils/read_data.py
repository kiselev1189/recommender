import pandas as pd
from sklearn.cross_validation import KFold
import numpy as np

def clean_dataframe(df):
    type_count = {}
    for column in df.columns:

        if df[column].dtype not in type_count:
            type_count[df[column].dtype] = 0
        else:
            type_count[df[column].dtype] += 1

        print ('[%s]' % (df[column].dtype), df[column].name, df[column].describe())
        categories_count = len(df[column].value_counts())
        print ('\nWith %s unique values: \n' % (categories_count))

        if categories_count < pd.get_option('display.max_rows'):
            for value_name in df[column].value_counts().index:
                print (value_name, df[column].value_counts()[value_name],
                       float(df[column].value_counts()[value_name]) / len(df) * 100)
        else:
            print (df[column].value_counts())

        print ('\n')


def read_data(input_path="../input/", filename="orders_items_registrations.csv"):
    df = pd.read_csv(input_path + filename)
    df['order_date'] = pd.to_datetime(df['order_date'])
    df['fulfilment_date'] = pd.to_datetime(df['fulfilment_date'])
    df['user_joined'] = pd.to_datetime(df['user_joined'])
    df['user_activated'] = pd.to_datetime(df['user_activated'])
    return df


def train_test(df, test_size=0.1):
    df = df.sort_values('order_date', ascending=False)
    test_len = int(len(df)*test_size)
    train_len = int(len(df)*(1 - test_size))
    test = df[-test_len:]
    train = df[:train_len]
    return train, test


def user_item_matrix(df):
    unique_users = df["user_id"].unique()
    unique_sets = df["set_id"].unique()

    matrix = np.zeros((len(unique_users), len(unique_sets)))

    user_id_index = {user_id: i for (user_id, i) in zip(unique_users, range(0, len(unique_users)))}
    set_id_index = {set_id: i for (set_id, i) in zip(unique_sets, range(0, len(unique_sets)))}
    index_user_id = {i: user_id for (user_id, i) in zip(unique_users, range(0, len(unique_users)))}
    index_set_id = {i: set_id for (set_id, i) in zip(unique_sets, range(0, len(unique_sets)))}

    for row in df.iterrows():
        order = row[1]
        user_id = order["user_id"]
        set_id = order["set_id"]
        matrix[user_id_index[user_id]][set_id_index[set_id]] += order["quantity_of_this_set"]

    return matrix, index_user_id, index_set_id