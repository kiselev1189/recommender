# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
from pandas.tseries.offsets import *
import math


def identify_successors(df):
    set_successors = {int(key): [] for key in df["set_id"].unique()}

    for row_number, row in df.iterrows():
        id = int(row["set_id"])
        successor = row["set_successor"]
        if math.isnan(successor):
           continue
        else:
            successor = int(successor)

        set_successors[id].append(successor)

    for key in set_successors:
        set_successors[key] = list(set(set_successors[key]))

    set_successors = {key: set_successors[key] for key in set_successors if set_successors[key] != []}

    for row_number, row in df.iterrows():
        set_id = row["set_id"]
        if int(set_id) in set_successors.keys():
            df.set_value(row_number, "set_id", int(set_successors[set_id][-1]))

    return df


def identify_named_successors(df):
    names = list(df["set_title"].unique())
    namebase_setids = {i.split(" v.")[0].strip(): [] for i in names if " v." in i}

    for number, row in df.iterrows():
        for i in namebase_setids:
            if i in row["set_title"] and row["set_id"] not in namebase_setids[i]:
                namebase_setids[i].append(row["set_id"])

    namebase_setids = {i: list(sorted(namebase_setids[i], reverse=True))[0] for i in namebase_setids.keys()}
    for row_number, row in df.iterrows():
        base = row["set_title"].split(" v.")[0].strip()
        if base in namebase_setids.keys():
            df.set_value(row_number, "set_id", namebase_setids[base])

    return df


def filter_new(train, test):
    """Removes previously unseen users from test set.

    Parameters
    ----------
    train : pandas.dataframe
    test : pandas.dataframe

    Returns
    -------
    test : pandas.dataframe
        Filtered dataframe for users which are new.

    """

    train_ids = train["user_id"].unique()
    test = test[test["user_id"].isin(train_ids)]
    print "Users:", len(train["user_id"].unique())
    return test


def clean_sold_out(df, test_start, offset):
    """Cleans out sets, which were moved out of sale.

    Parameters
    ----------
    df : pandas.dataframe
        Input dataframe.
    test_start : dt.datetime
        When test set starts.
    offset : dt.datetime
        Offset to make sure we cense the set right.

    Returns
    -------
    df : pandas.dataframe
        Cleaned dataframe

    """

    sales_start = test_start - offset
    sales_end = test_start
    current_sets = df[(df["order_date"] >= sales_start) & (df["order_date"] <= sales_end)]
    current_sets = current_sets["set_id"].unique()
    print "Active sets:", len(current_sets)
    df = df[df["set_id"].isin(current_sets)]
    return df


def remove_new_sets_from_test(train, test):
    sets_in_train = train["set_id"].unique()
    clean_test = test[test["set_id"].isin(sets_in_train)]
    return clean_test


def clean_dataframe(df):
    """Cleans dataframe
    """
    type_count = {}
    for column in df.columns:

        if df[column].dtype not in type_count:
            type_count[df[column].dtype] = 0
        else:
            type_count[df[column].dtype] += 1

        print ('[%s]' % (df[column].dtype), df[column].name, df[column].describe())
        categories_count = len(df[column].value_counts())
        print ('\nWith %s unique values: \n' % (categories_count))

        if categories_count < pd.get_option('display.max_rows'):
            for value_name in df[column].value_counts().index:
                print (value_name, df[column].value_counts()[value_name],
                       float(df[column].value_counts()[value_name]) / len(df) * 100)
        else:
            print (df[column].value_counts())

        print ('\n')


def read_data(input_path="../input/", filename="orders_items_registrations.csv"):
    """Reads data from input file and returns dataframe

    Parameters
    ----------
    input_path : str
        Path to file, optional
    filename : str
        Filename to read, optional

    Returns
    -------
    df : pandas.dataframe

    """
    df = pd.read_csv(input_path + filename, encoding="utf-8")
    df['order_date'] = pd.to_datetime(df['order_date'])
    df['fulfilment_date'] = pd.to_datetime(df['fulfilment_date'])
    df['user_joined'] = pd.to_datetime(df['user_joined'])
    df['user_activated'] = pd.to_datetime(df['user_activated'])
    return df


def train_test(df, test_size=DateOffset(months=1, days=15), sales_end_offset=DateOffset(months=0, days=20)):
    """Splits input dataframe for train and test.

    Parameters
    ----------
    df : pandas.dataframe
    test_size : DateOffset
        Sets offset for test set.
    sales_end_offset: DateOffset
        Sets offset when we consider sales ended.

    Returns
    -------
    train, test : tuple
        Train and test sets.

    """

    df = df.sort_values(by='order_date', ascending=True)
    df = df[df["set_price"] > 0.0]  # removes free glasses from data
    df = identify_successors(df)
    df = identify_named_successors(df)
    test_start = df["order_date"].iloc[-1] - test_size
    df = clean_sold_out(df, test_start=test_start, offset=sales_end_offset)
    df = df[~df["set_title"].str.contains(u"Сертификат")]
    test = df[df["order_date"] > test_start]
    train = df[df["order_date"] <= test_start]
    test = remove_new_sets_from_test(train, test)

    print "Test size:", len(test),  "Train size", len(train)
    return train, test


def user_item_matrix(df):
    """Constructs User-Item matrix from dataframe

    Parameters
    ----------
    df : pandas.dataframe
        Input dataframe

    Returns
    -------
    matrix, index_user_id, index_set_id : tuple


    """
    unique_users = df["user_id"].unique()
    unique_sets = df["set_id"].unique()

    matrix = np.zeros((len(unique_users), len(unique_sets)))

    user_id_index = {user_id: i for (user_id, i) in zip(unique_users, range(0, len(unique_users)))}
    set_id_index = {set_id: i for (set_id, i) in zip(unique_sets, range(0, len(unique_sets)))}
    index_user_id = {i: user_id for (user_id, i) in zip(unique_users, range(0, len(unique_users)))}
    index_set_id = {i: set_id for (set_id, i) in zip(unique_sets, range(0, len(unique_sets)))}

    for row in df.iterrows():
        order = row[1]
        user_id = order["user_id"]
        set_id = order["set_id"]
        matrix[user_id_index[user_id]][set_id_index[set_id]] += order["quantity_of_this_set"]

    return matrix, index_user_id, index_set_id

