from read_data import train_test, read_data, user_item_matrix, filter_new
from eval_metrics import apk, top_n
import recsys
from recsys.algorithm.factorize import SVD
from recsys.datamodel.data import Data
from sklearn.feature_extraction import DictVectorizer
from sklearn.metrics import mean_squared_error
from sklearn.cluster import SpectralClustering
import numpy as np
from scipy.spatial.distance import cosine
recsys.algorithm.VERBOSE = True


def convert_to_recsys_data(df):
    """Converts data to recsys format.

    Parameters
    ----------
    df : pandas.dataframe
        Dataframe with User-Item information

    Returns
    -------
    data : recsys.datamodel.data
        Data, represented in internal recsys format

    """

    matrix, index_user_id, index_set_id = user_item_matrix(df)
    data_tuples = []
    for i in np.ndindex(matrix.shape):
        if matrix[i] != 0:
            data_tuples.append((matrix[i], index_user_id[i[0]], index_set_id[i[1]]))
    data = Data()
    data.set(data_tuples)
    return data


class MostPopularRecommender:
    """Naive recommender for most popular winesets.

    Recommends just top sales.
    """

    def __init__(self, data, count=6):
        self.most_popular = self.get_most_popular(data, count=count)

    def get_most_popular(self, data, count):
        set_ids = list(set([i[2] for i in data]))
        set_count = {i: 0 for i in set_ids}
        for i in data:
            set_count[i[2]] += i[0]
        set_count = [[set_count[i], i] for i in set_count]
        set_count.sort(reverse=True)
        most_popular = [i[1] for i in set_count[:count]]
        return most_popular

    def evaluate(self, test, metric=top_n):
        user_ids = list(set([i[1] for i in test]))
        user_purchases = {i: [] for i in user_ids}

        for order in test:
            user_purchases[order[1]].append(order[2])

        for user_id in user_ids:
            user_purchases[user_id] = list(set(user_purchases[user_id]))

        quality = []
        for user_id in user_ids:
            rec_set_ids = self.most_popular
            true_set_ids = user_purchases[user_id]
            quality.append(metric(true_set_ids, rec_set_ids))

        return sum(quality) / len(user_ids)

    def recommend(self, user_id):
        tuples = []
        for i in self.most_popular:
            set_id = i
            tuples.append((user_id, set_id))
        return tuples


class Recommender:
    """Actual recsys recommender, based on SVD
    """

    def __init__(self, data, k=10, most_pop_count=6):
        self.data = data
        self.svd = SVD()
        self.svd.set_data(data)
        self.svd.compute(k=k)
        self.most_pop_count = most_pop_count


    def evaluate(self, test, metric=top_n):
        user_ids = list(set([i[1] for i in test]))
        user_purchases = {i: [] for i in user_ids}

        for order in test:
            user_purchases[order[1]].append(order[2])

        for user_id in user_ids:
            user_purchases[user_id] = list(set(user_purchases[user_id]))

        quality = []
        for user_id in user_ids:
            recommendation = self.svd.recommend(user_id, is_row=True, n=6)
            rec_set_ids = [i[0] for i in recommendation]
            true_set_ids = user_purchases[user_id]

            quality.append(metric(true_set_ids, rec_set_ids))

        return sum(quality) / len(user_ids)

    def recommend(self, user_id):
        recommendation = self.svd.recommend(user_id, is_row=True, n=6)
        tuples = []
        for i in recommendation:
            rating = i[1]
            set_id = i[0]
            tuples.append((user_id, set_id, rating))

        return tuples

    def clusterize(self):
        # return dict of following structure {cluster_id: {"set_ids":[], "user_ids": []}}
        S = self.svd._S # diagonal, only need it for len (len(S) is actual count of clusters)
        U = self.svd._U # user-to-cluster
        V = self.svd._V # wine-to-cluster
        clusters = {cluster_id: {"set_ids":[], "user_ids": []} for cluster_id in range(0, len(S))}

        for user_id in U.row_labels:
            row = [abs(i) for i in list(U.row_named(user_id))]
            cluster_id = row.index(max(row))
            clusters[cluster_id]["user_ids"].append(user_id)

        V = V.T
        for cluster_id in clusters:
            cluster_row = V[cluster_id]
            for i in range(0, len(cluster_row)):
                cluster_row[i] = abs(cluster_row[i])
            top_sets = [i[0] for i in cluster_row.top_items(n=6)]
            clusters[cluster_id]["set_ids"] = top_sets

        return clusters


    def eval_on_clusters(self, test, metric=top_n, from_svd=3, from_top=3):
        cluster_dict = self.top_svd_mixed_clusters(from_svd=from_svd, from_top=from_top)
        user_ids = list(set([i[1] for i in test]))
        user_purchases = {i: [] for i in user_ids}

        for order in test:
            user_purchases[order[1]].append(order[2])

        for user_id in user_ids:
            user_purchases[user_id] = list(set(user_purchases[user_id]))

        quality = []
        for user_id in user_ids:
            for cluster in cluster_dict:
                if user_id in cluster_dict[cluster]["user_ids"]:
                    recommendation = cluster_dict[cluster]["set_ids"]

            rec_set_ids = recommendation
            true_set_ids = user_purchases[user_id]
            quality.append(metric(true_set_ids, rec_set_ids))

        return sum(quality) / len(user_ids)

    def top_svd_mixed_clusters(self, from_svd=3, from_top=3):
        most_pop = MostPopularRecommender(self.data, count=(from_top+from_svd*2)).most_popular # top 3
        # return dict of following structure {cluster_id: {"set_ids":[], "user_ids": []}}
        S = self.svd._S  # diagonal, only need it for len (len(S) is actual count of clusters)
        U = self.svd._U  # user-to-cluster
        V = self.svd._V  # wine-to-cluster
        clusters = {cluster_id: {"set_ids": [], "user_ids": []} for cluster_id in range(0, len(S))}

        U = U*S
        for user_id in U.row_labels:
            row = list(U.row_named(user_id))
            cluster_id = row.index(max(row))
            clusters[cluster_id]["user_ids"].append(user_id)

        V = V.T
        for cluster_id in clusters:
            cluster_row = V[cluster_id]
            top_sets = [i[0] for i in cluster_row.top_items(n=from_svd)]
            clusters[cluster_id]["set_ids"] = top_sets

            for set_id in most_pop:
                if set_id not in clusters[cluster_id]["set_ids"] and len(clusters[cluster_id]["set_ids"]) < from_svd + from_top:
                    clusters[cluster_id]["set_ids"].append(set_id)

        return clusters


class SpectralClusteringRecommender:

    def __init__(self, data, k=0, most_pop_count=6):
        self.data = data
        self.svd = SVD()
        self.svd.set_data(data)
        self.svd.compute(k=k)
        self.most_pop_count = most_pop_count

    def clusterize(self):
        S = self.svd._S
        U = self.svd._U
        V = self.svd._V

        svd_matrix = np.asarray(self.svd._matrix_reconstructed.to_dense())
        print svd_matrix
        user_row = {user_id: i for user_id, i in zip(U.row_labels, range(0, len(U)))}
        row_user = {i: user_id for user_id, i in zip(U.row_labels, range(0, len(U)))}
        similarity_matrix = np.zeros((len(U.row_labels), len(U.row_labels)))

        for i in range(0, len(svd_matrix)):
            for j in range(0, len(svd_matrix)):
                similarity_matrix[i][j] = cosine(svd_matrix[i], svd_matrix[j])

        #for user_id_1 in U.row_labels:
        #    for user_id_2 in U.row_labels:
        #        similarity_matrix[user_row[user_id_1]][user_row[user_id_2]] = cosine(self.svd._get_row_reconstructed(user_id_1), self.svd._get_row_reconstructed(user_id_2))
        print similarity_matrix
        print "clustering"
        cls = SpectralClustering(n_clusters=3, affinity="precomputed")
        cluster_labels = cls.fit_predict(similarity_matrix)
        print cluster_labels



class CrabRecommender():
    """Actual crab recommender, based on SVD
    """

    def __init__(self, train, k=10, verbose=False):
        internal_train = {}
        for i, item in enumerate(train):
            internal_train[item[1]] = {}

        for i, item in enumerate(train):
            internal_train[item[1]][item[2]] = float(item[0])

        self.v = DictVectorizer()
        X_train = self.v.fit_transform(internal_train)
        y = np.array(y_train)
        self.fm = pylibfm.FM(num_factors=k, num_iter=100, verbose=verbose, task="regression",
                    initial_learning_rate=0.001, learning_rate_schedule="optimal")
        self.fm.fit(X_train,y)

    def evaluate(self, test):
        pmf_test = []
        y_test = []
        for i, item in enumerate(test):
            pmf_test.append({"user": str(item[1]), "item": str(item[2])})
            y_test.append(float(item[0]))

        X_test = self.v.transform(pmf_test)
        preds = self.fm.predict(X_test)

        return mean_squared_error(y_test,preds)
