import os

import pandas as pd
from read_utils import train_test, user_item_matrix_time_weighted, preprocess_memory_based
from pandas.tseries.offsets import DateOffset
import sklearn.externals.joblib as jl
from fastFM import als


def calc(fold_size, input_file_path, current_date, RecommenderClass, k):
    from read_utils import train_test, user_item_matrix_time_weighted
    from pandas.tseries.offsets import DateOffset
    from json import dumps
    from recommender import evaluate

    # print "calc"

    train, test = train_test(input_file_path, current_date, test_size=fold_size, active_sets_offset=DateOffset(days=7))
    # train, test = train_test(input_file_path=None, test_size=fold_size, active_sets_offset=DateOffset(days=7),
    #                          df=df[df["order_date"] < current_date])

    print RecommenderClass, len(train), len(test)
    
    if RecommenderClass.rec_type in ['clustering_rec', 'most_popular_rec', 'sec_most_popular_rec', 'svd_rec']:
        X, index_user_id, index_set_id = user_item_matrix_time_weighted(train, time_func=lambda x, p: 1, time_func_params=0)
        rec = RecommenderClass(X, train, index_user_id, index_set_id, k=k)
    elif RecommenderClass.rec_type == 'user_based':
        user_items_train, item_users_train = preprocess_memory_based(train)
        rec = RecommenderClass(user_items_train)
    elif RecommenderClass.rec_type == 'item_based':
        user_items_train, item_users_train = preprocess_memory_based(train)
        rec = RecommenderClass(user_items_train, item_users_train)
    elif RecommenderClass.rec_type == 'fm_rec':
        fm = als.FMClassification(n_iter=200, init_stdev=0.001, rank=10, l2_reg_w=2, l2_reg_V=2)
        rec = RecommenderClass(fm, n_recommenders = 20)
        df_train = train[['user_id', 'item_set_id']]
        rec.train(df_train, neg_sample_alpha =10)
    elif RecommenderClass.rec_type == 'fm_context_rec':
        fm = als.FMClassification(n_iter=200, init_stdev=0.001, rank=10, l2_reg_w=2, l2_reg_V=2)
        rec = RecommenderClass(fm, n_recommenders = 20)
        df_train = train[['user_id', 'item_set_id', 'user_gender']]
        df_train.loc[df_train['user_gender'] == 'f', 'user_gender'] = 0
        df_train.loc[df_train['user_gender'] == 'm', 'user_gender'] = 1
        df_train.loc[df_train['user_gender'].isnull(), 'user_gender'] = 2
    
    accuracy = evaluate(rec, test)
    train_end_date = train["order_date"].max()

    # fold_accuracies.append((train_end_date, accuracy))

    with open('test', 'a') as f:
        f.write(dumps({'RecommenderClass': str(RecommenderClass),
                            'train_end_date': str(train_end_date),
                            'accuracy': accuracy}) + '\n')


def rolling_crossval_draft(input_file_path, cur_time, RecommenderClass, k=6, step=DateOffset(months=0, days=7),
                           fold_size=DateOffset(months=1), start_offset=DateOffset(years=1, months=0, days=0),
                           time_func=lambda x, p: 1, time_func_params=0):

    # from json import loads

    # print "rolling_crossval_draft", input_file_path, cur_time, RecommenderClass, k, step, fold_size, time_func, time_func_params

    # df["order_date"] = pd.to_datetime(df["order_date"])
    # last_step_start = df["order_date"].max() - fold_size
    # current_date = df["order_date"].max() - start_offset + fold_size
    # df = df[df["set_price"] > 0.0]

    current_date = cur_time - start_offset + fold_size
    last_step_start = cur_time - fold_size

    # fold_accuracies = []

    current_dates = list()
    while current_date <= last_step_start:
        current_dates.append(current_date)
        current_date += step

    # print last_step_start, current_dates, current_date

    jl.Parallel(n_jobs=-1)(jl.delayed(calc)(fold_size, input_file_path, current_date,
                                            RecommenderClass, k)
                           for current_date in current_dates)
    # fold_size, df, current_date, fold_accuracies, RecommenderClass, k
    # jl.Parallel(n_jobs=-1)(jl.delayed(process_groups)(i, end_date) for i in range(n_buckets))

    # while current_date <= last_step_start:
    #     train, test = train_test(input_file_path=None, test_size=fold_size, active_sets_offset=DateOffset(days=7),
    #                              df=df[df["order_date"] < current_date])
    #
    #     print len(train), len(test)
    #
    #     X, index_user_id, index_set_id = user_item_matrix_time_weighted(train, time_func=time_func, time_func_params=time_func_params)
    #     rec = RecommenderClass(X, index_user_id, index_set_id, k=k)
    #     accuracy = rec.evaluate(test)
    #     train_end_date = train["order_date"].max()
    #     fold_accuracies.append((train_end_date, accuracy))
    #     current_date += step


def rolling_crossval(df, RecommenderClass, k=6, step=DateOffset(months=0, days=7), fold_size=DateOffset(months=1),
                     start_offset=DateOffset(years=1, months=0, days=0), time_func=lambda x, p: 1, time_func_params=0):
    df["order_date"] = pd.to_datetime(df["order_date"])
    last_step_start = df["order_date"].max() - fold_size
    current_date = df["order_date"].max() - start_offset + fold_size
    df = df[df["set_price"] > 0.0]

    fold_accuracies = []

    while current_date <= last_step_start:
        train, test = train_test(input_file_path=None, test_size=fold_size, active_sets_offset=DateOffset(days=7),
                                 df=df[df["order_date"] < current_date])

        print len(train), len(test)

        X, index_user_id, index_set_id = user_item_matrix_time_weighted(train, time_func=time_func, time_func_params=time_func_params)
        rec = RecommenderClass(X, index_user_id, index_set_id, k=k)
        accuracy = rec.evaluate(test)
        train_end_date = train["order_date"].max()
        fold_accuracies.append((train_end_date, accuracy))
        current_date += step
        with open('test', 'a') as f:
            f.write('{}, {}, {}\n'.format(RecommenderClass, train_end_date, accuracy))

    return fold_accuracies
