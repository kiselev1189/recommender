from eval_metrics import top_n
import numpy as np
import pandas as pd
from kmedoids import KMedoids
from scipy.stats import entropy
from numpy.linalg import norm
from sklearn.utils.extmath import randomized_svd
from sklearn.preprocessing import OneHotEncoder


def JSD(P, Q):
    _P = P / norm(P, ord=1)
    _Q = Q / norm(Q, ord=1)
    _M = 0.5 * (_P + _Q)
    return 0.5 * (entropy(_P, _M) + entropy(_Q, _M))


def skjsd(X):
    from scipy.spatial.distance import pdist
    return pdist(X, metric=JSD)


def evaluate(rec, test, metric=top_n):
    acc = []
    for user in test["user_id"].unique():
        user_actual = list(test[test["user_id"] == user]["item_set_id"])
        if user_actual != []:
            acc.append(metric(user_actual, rec.predict(user)))
    return sum(acc) / len(acc)


def evaluate_draft(rec, test, metric=top_n):
    acc = []
    for user in test["user_id"].unique():
        user_actual = list(test[test["user_id"] == user]["item_set_id"])
        if user_actual != []:
            if rec.rec_type == 'fm_context_rec':
                factors = test.loc[test["user_id"] == user, rec.factors[2:]].iloc[0].tolist()
                acc.append(metric(user_actual, rec.predict(user)))
            else:
                acc.append(metric(user_actual, rec.predict(user, factors)))
    return sum(acc) / len(acc)


class ClusteringRecommender():
    rec_type = 'clustering_rec'

    def __init__(self, X, train, index_user_id, index_set_id, k=10):
        """Recommender initialization

        Algorithm steps:
            1) Reconstruct user-item matrix with SVD truncated at k = number of items
            2) Perform K-Medoids clustering on user preference vectors
            3) Assign recommendations to each cluster according to medoid preference vector, mix in some best sellers

        Parameters
        ----------
        X : np.array
        User - item matrix

        index_user_id : dict
        Mapping of indexes to users

        index_set_id : dict
        Mapping of indexes to set_ids

        """

        self.index_user_id = index_user_id
        self.index_set_id = index_set_id
        U, s, V = np.linalg.svd(X, full_matrices=False)
        # U, s, V = randomized_svd(X, n_components=6, random_state=None)
        self.reconstructed_matrix = np.dot(U, np.dot(np.diag(s), V))
        self.k = k
        self.most_pop = self.get_most_pop(X, index_set_id)

    def get_most_pop(self, X, index_set_id):
        popularity_vector = np.sum(X, axis=0)
        most_pop_sets = [index_set_id[i] for i in np.argsort(popularity_vector)[::-1]][:12]
        return most_pop_sets

    def clusterize(self, from_kmedoids=4, from_top=2):
        """ Performs clusterization
        Parameters
        ----------
        from_kmedoids : int

        from_top : int

        Returns
        -------
        cluster_users: dict
        Mapping of clusters to lists of users

        cluster_recs: dict
        Mapping of clusters to lists of recommended sets
        """

        clusterizer = KMedoids(n_clusters=self.k,
                               distance_metric='euclidean')  # TODO: try to compute with various metrics
        clusterizer.fit(self.reconstructed_matrix)

        get_kmeans_recs = lambda vector: [self.index_set_id[i] for i in np.argsort(vector)[::-1][:from_kmedoids]]
        cluster_recs = {i: get_kmeans_recs(centroid) for i, centroid in enumerate(clusterizer.cluster_centers_)}

        if from_top > 0:
            for i, recs in cluster_recs.items():
                recs.extend([item for item in self.most_pop if item not in recs][:from_top])

        cluster_users = {i: [self.index_user_id[j] for j, cluster in enumerate(clusterizer.labels_) if cluster == i]
                         for i in set(clusterizer.labels_)}

        return cluster_users, cluster_recs

    def evaluate(self, test, from_kmedoids=4, from_top=2, metric=top_n):
        """ Performs clusterization and evaluates it on test data
            Parameters
            ----------
            test : dataframe

            from_kmedoids : int

            from_top : int

            metric : callable

            Returns
            -------
            performance : float
            Averaged metric value
        """

        cluster_users, cluster_recs = self.clusterize(from_kmedoids=from_kmedoids, from_top=from_top)
        acc = []
        for cluster in cluster_users:
            recs = cluster_recs[cluster]
            for user in cluster_users[cluster]:
                user_actual = list(test[test["user_id"] == user]["item_set_id"])
                if user_actual != []:
                    acc.append(metric(user_actual, recs))

        performance = sum(acc) / len(acc)
        return performance

    def save_clusterization(self, from_kmedoids=4, from_top=2, output_directory="output/"):
        """ Performs clusterization and saves it to csv
            Parameters
            ----------
            from_kmedoids : int

            from_top : int

            output_directory : str
        """

        cluster_users, cluster_recs = self.clusterize(from_kmedoids=from_kmedoids, from_top=from_top)

        for cluster, recs in cluster_recs.items():
            recfile_name = output_directory + "sets_" + str(cluster) + ".csv"

            with open(recfile_name, "w") as outfile:
                outfile.write("item_set_id" + "\n")
                for i in recs:
                    outfile.write(str(i) + "\n")

            userfile_name = output_directory + "users_" + str(cluster) + ".csv"

            with open(userfile_name, "w") as outfile:
                outfile.write("user_id" + "\n")
                for i in cluster_users[cluster]:
                    outfile.write(str(i) + "\n")


class MostPopularRecommender():
    rec_type = 'most_popular_rec'

    """Naive recommender for most popular winesets.

        Recommends just top sales.
        """

    def __init__(self, X, train, index_user_id, index_set_id, k=10, count=6):
        self.index_set_id = index_set_id
        self.index_user_id = index_user_id
        self.train = train
        self.most_popular = self.get_most_popular(X, count=count)

    def get_most_popular(self, X, count):
        popularity_vector = np.sum(X, axis=0)
        most_pop_sets = [self.index_set_id[i] for i in np.argsort(popularity_vector)[::-1]][:count]
        return most_pop_sets

    def predict(self, user):
        return self.most_popular

        # def evaluate(self, test, metric=top_n):
        #     acc = []
        #     for user in test["user_id"].unique():
        #         user_actual = list(test[test["user_id"] == user]["item_set_id"])[1:]
        #         if user_actual != []:
        #             acc.append(metric(user_actual, self.most_popular))
        #     return sum(acc) / len(acc)


class SecondaryMostPopularRecommender(MostPopularRecommender):
    rec_type = 'sec_most_popular_rec'

    def get_most_popular(self, X, count):
        from read_utils import secondary_purchases_top
        return secondary_purchases_top(self.train)[:count]


class SVDRecommender():
    rec_type = 'svd_rec'

    def __init__(self, X, train, index_user_id, index_set_id, k=6, from_svd=4, from_top=2):
        self.index_user_id = index_user_id
        self.index_set_id = index_set_id
        self.k = k
        self.most_pop = self.get_most_pop(X, index_set_id)

        self.U, self.S, self.V = randomized_svd(X, n_components=k, random_state=None)

        self.cluster_users, self.cluster_recs = self.clusterize(from_svd=from_svd, from_top=from_top)

    def get_most_pop(self, X, index_set_id):
        popularity_vector = np.sum(X, axis=0)
        most_pop_sets = [index_set_id[i] for i in np.argsort(popularity_vector)[::-1]][:12]
        return most_pop_sets

    def clusterize(self, from_svd=4, from_top=2):

        cluster_users = {i: [] for i in range(0, self.k)}
        for i, user in enumerate(self.U):
            cluster_id = np.argmax(user)
            cluster_users[cluster_id].append(self.index_user_id[i])

        cluster_recs = {i: [] for i in range(0, self.k)}
        for cluster_id in range(0, self.k):
            cluster_row = self.V[cluster_id]
            top_sets = [self.index_set_id[i] for i in np.argsort(cluster_row)[::-1][:from_svd]]
            if from_top > 0:
                top_sets.extend([i for i in self.most_pop if i not in top_sets][:from_top])
            cluster_recs[cluster_id] = top_sets

        return cluster_users, cluster_recs

    def predict(self, user):
        user_cluster = None
        for c, cluster in self.cluster_users.iteritems():
            if user in cluster:
                user_cluster = c
        if user_cluster:
            return self.cluster_recs[user_cluster]
        else:
            return []

    # def evaluate(self, test, from_svd=4, from_top=2, metric=top_n):
    #     """ Performs clusterization and evaluates it on test data
    #         Parameters
    #         ----------
    #         test : dataframe
    #
    #         from_kmedoids : int
    #
    #         from_top : int
    #
    #         metric : callable
    #
    #         Returns
    #         -------
    #         performance : float
    #         Averaged metric value
    #     """
    #
    #     cluster_users, cluster_recs = self.clusterize(from_svd=from_svd, from_top=from_top)
    #     acc = []
    #     for cluster in cluster_users:
    #         recs = cluster_recs[cluster]
    #         for user in cluster_users[cluster]:
    #             user_actual = list(test[test["user_id"] == user]["item_set_id"])
    #             if user_actual != []:
    #                 acc.append(metric(user_actual, recs))
    #
    #     performance = sum(acc) / len(acc)
    #     return performance

    def save_clusterization(self, from_svd=4, from_top=2, output_directory="output/"):
        """ Performs clusterization and saves it to csv
            Parameters
            ----------
            from_kmedoids : int

            from_top : int

            output_directory : str
        """

        cluster_users, cluster_recs = self.clusterize(from_svd=from_svd, from_top=from_top)

        for cluster, recs in cluster_recs.items():
            recfile_name = output_directory + "sets_" + str(cluster) + ".csv"

            with open(recfile_name, "w") as outfile:
                outfile.write("item_set_id" + "\n")
                for i in recs:
                    outfile.write(str(i) + "\n")

            userfile_name = output_directory + "users_" + str(cluster) + ".csv"

            with open(userfile_name, "w") as outfile:
                outfile.write("user_id" + "\n")
                for i in cluster_users[cluster]:
                    outfile.write(str(i) + "\n")


############### mpavlov recommender######


def KNN_user(target_user_id, users_places_dict_all,
             users_places_dict_to_rec=None,
             K=-1,
             recommend_visited=True):
    '''varian 1 of users filtration
       take into account all users'''

    if users_places_dict_to_rec is None:
        users_places_dict_to_rec = users_places_dict_all

    if target_user_id not in users_places_dict_all:  # no history for user -skip
        return {}

    target_user_places_all = users_places_dict_all[target_user_id]
    if target_user_id in users_places_dict_to_rec:
        target_user_places = users_places_dict_to_rec[target_user_id]
    else:
        target_user_places = set()
    # get all similarities
    users_sim = []
    for user_id in users_places_dict_to_rec.keys():
        if user_id == target_user_id:
            continue
        user_places_all = users_places_dict_all[user_id]
        places_union = target_user_places_all | user_places_all
        places_inter = target_user_places_all & user_places_all
        sim = 1. * len(places_inter) / len(places_union)
        if sim > 0:
            users_sim.append((user_id, sim))

    users_sim = sorted(users_sim, key=lambda x: x[1], reverse=True)

    if K > 0:
        users_sim = users_sim[:K]

    place_rating_dict = {}
    for user_id, sim in users_sim:
        if recommend_visited:
            places_to_rec = users_places_dict_to_rec[user_id]
        else:
            places_to_rec = users_places_dict_to_rec[user_id] - target_user_places
        for place_id in places_to_rec:
            place_rating_dict.setdefault(place_id, 0)
            place_rating_dict[place_id] += sim

    return sorted(place_rating_dict.items(), key=lambda x: x[1], reverse=True)


def KNN_item(target_user_id,
             target_user_places,
             places_users_dict_all,
             places_users_dict_to_req=None,
             K=-1,
             norm_by_places=False):
    '''item based recomendations'''

    if places_users_dict_to_req is None:
        places_users_dict_to_req = places_users_dict_all

    res = []
    for place_id, users_place in places_users_dict_to_req.iteritems():
        # skip places already visited
        if place_id in target_user_places:
            continue

        place_sims = []
        # iterate places of target user
        for target_place in target_user_places:
            if target_place in places_users_dict_all:
                users_target_place = places_users_dict_all[target_place]
            else:
                continue
            # calculate union and interaction
            union_len = len(users_place | users_target_place) - 1
            inter_set = (users_place & users_target_place)
            # remove target user from
            if target_user_id in inter_set:
                inter_set.remove(target_user_id)
            inter_len = len(inter_set)

            sim = 1. * inter_len / union_len if union_len > 0 else 0
            place_sims.append(sim)

        if K > 0:
            place_sims = sorted(place_sims, reverse=True)
            place_sims = place_sims[:K]

        if norm_by_places:
            place_sim = 1. * sum(place_sims) / len(place_sims)
        else:
            place_sim = sum(place_sims)

        res.append((place_id, place_sim))

    return sorted(res, key=lambda x: x[1], reverse=True)


class UserBasedRecommender:
    rec_type = 'user_based'

    def __init__(self, data):
        self.train_data = data

    def predict(self, test_user_id, top_N=6):

        res_items_score = KNN_user(test_user_id, self.train_data)
        res_items = [i[0] for i in res_items_score]
        res_items_topN = set(res_items[:top_N])

        return res_items_topN

    def evaluate(self, test_data, top_N=6):
        items_in_N = 0.

        for test_user_id in test_data.keys():

            # user based approach
            res_items_score = KNN_user(test_user_id, self.train_data)
            res_items = [i[0] for i in res_items_score]
            res_items_topN = set(res_items[:top_N])

            if len(test_data[test_user_id] & res_items_topN) > 0:
                items_in_N += 1.

        return items_in_N / len(test_data)


class ItemBasedRecommender:
    rec_type = 'item_based'

    def __init__(self, data_user_items, data_item_users):
        self.train_data_user_items = data_user_items
        self.train_data_item_users = data_item_users

    def predict(self, test_user_id, top_N=6):
        res_items_score = KNN_item(test_user_id,
                                   self.train_data_user_items[test_user_id],
                                   self.train_data_item_users)
        res_items = [i[0] for i in res_items_score]
        res_items_topN = set(res_items[:top_N])
        return res_items_topN

    def evaluate(self, test_data, top_N=6):
        items_in_N = 0.

        for test_user_id in test_data.keys():
            # item based approach
            res_items_score = KNN_item(test_user_id,
                                       self.train_data_user_items[test_user_id],
                                       self.train_data_item_users)
            res_items = [i[0] for i in res_items_score]
            res_items_topN = set(res_items[:top_N])

            if len(test_data[test_user_id] & res_items_topN) > 0:
                items_in_N += 1.

        return items_in_N / len(test_data)


class BaseRecommenderError(Exception): pass


class NotFittedError(BaseRecommenderError): pass


class FMRecommender:
    rec_type = 'fm_rec'

    def __init__(self, base_recommender, n_recommenders=10):

        self.n_recommenders = n_recommenders
        self.one_hot_enc = OneHotEncoder(handle_unknown='ignore')
        self.recommenders = []
        self._trained = False
        self.task = base_recommender.task

        # init recommenders
        klass = base_recommender.__class__
        rec_params = base_recommender.get_params().copy()
        for _ in xrange(n_recommenders):
            self.recommenders.append(klass(**rec_params))

    def train(self, df_train_pos, n_epochs=10, lr_start=0.001,
              neg_sample_alpha=10, lr_end=None, warm_start=False):

        self.train_data = df_train_pos

        # set learning rate
        if lr_end is None:
            lr_end = lr_start

        if n_epochs > 1:
            lr_step = 1. * (lr_start - lr_end) / (n_epochs - 1.)
        else:
            lr_step = 0

        # fit one hot encoder
        if not warm_start or not self._trained:
            self.one_hot_enc.fit(df_train_pos)

        for rec in self.recommenders:
            df_train_neg = self._negative_sampling(df_train_pos, neg_sample_alpha)
            df_train = df_train_pos.append(df_train_neg)[['user_id', 'item_set_id']]
            Y = self._get_Y_train(df_train_pos, df_train_neg, rec.task)
            X = self.one_hot_enc.transform(df_train)

            warm_start_rec = warm_start
            for epoch in range(n_epochs):
                # update learning rate
                lr = lr_start - epoch * lr_step
                # shuffle train data
                I = np.random.permutation(Y.shape[0])
                if rec.task == 'ranking':  # shuffle only Y_train(pairs)
                    Y_train = Y[I]
                    X_train = X
                else:  # suffle X_train and Ytrain
                    Y_train = Y[I]
                    X_train = X[I]
                # manage step size
                if rec.solver == 'sgd':
                    rec.fit(X_train, Y_train, n_iter=Y_train.shape[0], step_size=lr, warm_start=warm_start_rec)
                elif rec.solver == 'als':
                    rec.fit(X_train, Y_train, n_iter=n_epochs, warm_start=warm_start_rec)
                    break
                else:
                    raise ValueError('unknown solver')

                warm_start_rec = True

        self._trained = True

    def predict_(self, X):

        y_proba = self.predict_proba(X)
        if self.task == 'regression' or self.task == 'ranking':
            return y_proba
        else:
            y_pred = np.ones_like(y_proba, dtype=np.float64)
            y_pred[y_proba <= .5] = -1
            return y_pred

    def predict_proba(self, X):

        if not self._trained:
            raise NotFittedError('recommender was not fitted')

        X_one_hot = self.one_hot_enc.transform(X)
        predictions = []
        for rec in self.recommenders:
            if hasattr(rec, 'predict_proba'):
                predictions.append(rec.predict_proba(X_one_hot))
            else:
                predictions.append(rec.predict_(X_one_hot))

        return np.mean(predictions, axis=0)

    def evaluate(self, df_test, df_train, top_N=6):

        places_in_N = 0.

        usrs_cnt = 0
        for user_id, gr_user in df_test.groupby('user_id'):

            user_items_test = set(gr_user['item_set_id'])
            predicted_items = self.predict_places(user_id, df_train, top_N, True)

            if len(user_items_test & set(predicted_items[:10])) > 0:
                places_in_N += 1.

            usrs_cnt += 1

        return places_in_N / usrs_cnt

    def predict(self, user_id, data=None, num_places=6, predict_visited=True):

        if data is None:
            data = self.train_data

        all_places = set(data['item_set_id'].unique())
        user_places = set(data[data['user_id'] == user_id]['item_set_id'])

        if predict_visited:
            places_to_score = all_places
        else:
            places_to_score = all_places - user_places

        user_places = np.empty((len(places_to_score), 2), dtype=int)
        for i, place_id in enumerate(places_to_score):
            user_places[i] = (user_id, place_id)

        scores = self.predict_proba(user_places)
        top_indices = scores.argsort()[::-1][:num_places]

        return user_places[top_indices, 1]

    def _negative_sampling(self, data, alpha):

        all_places = set(data['item_set_id'].unique())

        negative_samples = []
        for user_id, gr_user in data.groupby('user_id'):
            user_places = set(gr_user['item_set_id'])
            places_to_sample = list(all_places - user_places)
            n_samples = min(int(round(alpha * len(user_places))), len(places_to_sample))

            if n_samples > 0:
                sample_places = np.random.choice(places_to_sample, n_samples, replace=False)
            elif alpha < 0:
                sample_places = places_to_sample
            else:
                continue

            for place_id in sample_places:
                row = {'item_set_id': place_id, 'user_id': user_id}
                negative_samples.append(row)

        return pd.DataFrame(negative_samples)

    def _get_Y_train(self, df_train_pos, df_train_neg, task):

        pos_len = df_train_pos.shape[0]
        neg_len = df_train_neg.shape[0]

        # for ranking do pairs
        if task == 'ranking':
            Y_train = []
            for user_id in df_train_pos.fk_user_id.unique():
                pos_iloc = np.where(df_train_pos.fk_user_id == user_id)[0]
                neg_iloc = np.where(df_train_neg.fk_user_id == user_id)[0]
                for pos_i in pos_iloc:
                    for neg_i in neg_iloc:
                        Y_train.append((pos_i, neg_i + pos_len))
            Y_train = np.array(Y_train)
        # for regression and classification do {-1, 1} encoding
        else:
            Y_train = np.ones(pos_len + neg_len)
            Y_train[pos_len:] = -1

        return Y_train


class FMContextRecommender:
    rec_type = 'fm_context_rec'

    def __init__(self, base_recommender, n_recommenders=10,
                 factors=['user_id', 'item_set_id', 'user_gender', 'order_month']):

        self.n_recommenders = n_recommenders
        self.one_hot_enc = OneHotEncoder(handle_unknown='ignore')
        self.recommenders = []
        self._trained = False
        self.task = base_recommender.task
        self.factors = factors

        # init recommenders
        klass = base_recommender.__class__
        rec_params = base_recommender.get_params().copy()
        for _ in xrange(n_recommenders):
            self.recommenders.append(klass(**rec_params))

    def train(self, df_train_pos, n_epochs=10, lr_start=0.001,
              neg_sample_alpha=10, lr_end=None, warm_start=False):

        self.train_data = df_train_pos

        # set learning rate
        if lr_end is None:
            lr_end = lr_start

        if n_epochs > 1:
            lr_step = 1. * (lr_start - lr_end) / (n_epochs - 1.)
        else:
            lr_step = 0

        # fit one hot encoder
        if not warm_start or not self._trained:
            self.one_hot_enc.fit(df_train_pos)

        for rec in self.recommenders:
            df_train_neg = self._negative_sampling(df_train_pos, neg_sample_alpha)
            df_train = df_train_pos.append(df_train_neg)[self.factors]
            Y = self._get_Y_train(df_train_pos, df_train_neg, rec.task)
            X = self.one_hot_enc.transform(df_train)

            warm_start_rec = warm_start
            for epoch in range(n_epochs):
                # update learning rate
                lr = lr_start - epoch * lr_step
                # shuffle train data
                I = np.random.permutation(Y.shape[0])
                if rec.task == 'ranking':  # shuffle only Y_train(pairs)
                    Y_train = Y[I]
                    X_train = X
                else:  # suffle X_train and Ytrain
                    Y_train = Y[I]
                    X_train = X[I]
                # manage step size
                if rec.solver == 'sgd':
                    rec.fit(X_train, Y_train, n_iter=Y_train.shape[0], step_size=lr, warm_start=warm_start_rec)
                elif rec.solver == 'als':
                    rec.fit(X_train, Y_train, n_iter=n_epochs, warm_start=warm_start_rec)
                    break
                else:
                    raise ValueError('unknown solver')

                warm_start_rec = True

        self._trained = True

    def predict_(self, X):

        y_proba = self.predict_proba(X)
        if self.task == 'regression' or self.task == 'ranking':
            return y_proba
        else:
            y_pred = np.ones_like(y_proba, dtype=np.float64)
            y_pred[y_proba <= .5] = -1
            return y_pred

    def predict_proba(self, X):

        if not self._trained:
            raise NotFittedError('recommender was not fitted')

        X_one_hot = self.one_hot_enc.transform(X)
        predictions = []
        for rec in self.recommenders:
            if hasattr(rec, 'predict_proba'):
                predictions.append(rec.predict_proba(X_one_hot))
            else:
                predictions.append(rec.predict_(X_one_hot))

        return np.mean(predictions, axis=0)

    def evaluate(self, df_test, df_train=None, top_N=6):

        places_in_N = 0.

        usrs_cnt = 0
        for user_id, gr_user in df_test.groupby('user_id'):

            user_items_test = set(gr_user['item_set_id'])

            factors = []
            for f_name in self.factors[2:]:
                factors.append(gr_user[f_name].iloc[0])

            predicted_items = self.predict(user_id, factors,
                                           df_train, top_N, True)

            if len(user_items_test & set(predicted_items[:top_N])) > 0:
                places_in_N += 1.

            usrs_cnt += 1

        return places_in_N / usrs_cnt

    def predict(self, user_id, add_factors,
                data=None, num_places=6, predict_visited=False):

        if data is None:
            data = self.train_data

        all_places = set(data['item_set_id'].unique())
        user_places = set(data[data['user_id'] == user_id]['item_set_id'])

        if predict_visited:
            places_to_score = all_places
        else:
            places_to_score = all_places - user_places

        user_places = np.empty((len(places_to_score), len(self.factors)), dtype=int)
        for i, place_id in enumerate(places_to_score):
            features = [user_id, place_id]
            features.extend(add_factors)
            user_places[i] = features

        scores = self.predict_proba(user_places)
        top_indices = scores.argsort()[::-1][:num_places]

        return user_places[top_indices, 1]

    def _negative_sampling(self, data, alpha):

        all_places = set(data['item_set_id'].unique())

        negative_samples = []
        for user_id, gr_user in data.groupby('user_id'):
            user_places = set(gr_user['item_set_id'])
            places_to_sample = list(all_places - user_places)
            n_samples = min(int(round(alpha * len(user_places))), len(places_to_sample))

            # get factors values (excepts user id, set id)
            add_factors = {}
            for factor_name in self.factors[2:]:
                add_factors[factor_name] = gr_user[factor_name].iloc[0]

            if n_samples > 0:
                sample_places = np.random.choice(places_to_sample, n_samples, replace=False)
            elif alpha < 0:
                sample_places = places_to_sample
            else:
                continue

            for place_id in sample_places:
                row = {'item_set_id': place_id, 'user_id': user_id}
                row.update(add_factors)
                negative_samples.append(row)

        return pd.DataFrame(negative_samples)

    def _get_Y_train(self, df_train_pos, df_train_neg, task):

        pos_len = df_train_pos.shape[0]
        neg_len = df_train_neg.shape[0]

        # for ranking do pairs
        if task == 'ranking':
            Y_train = []
            for user_id in df_train_pos.fk_user_id.unique():
                pos_iloc = np.where(df_train_pos.fk_user_id == user_id)[0]
                neg_iloc = np.where(df_train_neg.fk_user_id == user_id)[0]
                for pos_i in pos_iloc:
                    for neg_i in neg_iloc:
                        Y_train.append((pos_i, neg_i + pos_len))
            Y_train = np.array(Y_train)
        # for regression and classification do {-1, 1} encoding
        else:
            Y_train = np.ones(pos_len + neg_len)
            Y_train[pos_len:] = -1

        return Y_train
