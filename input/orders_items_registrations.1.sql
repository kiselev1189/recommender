SELECT
  o.id as order_id,                         # order id
  o.user_id as user_id,                     # user id
  o.date as order_date,                     # order date
  o.date_end as fulfilment_date,            # fulfilment date
  o.status as order_status,                 # order status
  o.summ as order_amount,                   # order amount
  o.first_order as is_the_first_order,      # is the first order
  iso.itemset_id as set_id,                 # set id
  iso.quantity as quantity_of_this_set,     # quantity of this set
  iso.summ as amount_for_this_set,          # amount for this set
  ps.title as set_title,                    # set title
  ps.price as set_price,                    # set price
  ps.small_description as set_short_desc,   # set short description
  ps.description_from_set as set_long_desc, # set long description
  ps.date as set_start_of_sales,            # set start of sales
  ps.successor_id as set_successor,         # set successor
  au.date_joined as user_joined,            # user joined
  au.date_activate as user_activated        # user activated
FROM
  core_order AS o,
  core_itemset_order AS iso,
  core_partofset AS ps,
  auth_user au
WHERE
  o.id = iso.order_id AND
  ps.id = iso.itemset_id AND
  au.id = o.user_id
ORDER BY
  o.id
