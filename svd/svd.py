import numpy as np
from read_data import train_test, read_data, user_item_matrix
from eval_metrics import apk
import recsys
from recsys.algorithm.factorize import SVD
from recsys.datamodel.data import Data
recsys.algorithm.VERBOSE = True


def filter_new(train, test):
    # removes previously unseen users from test set
    train_ids = train["user_id"].unique()
    test = test[test["user_id"].isin(train_ids)]
    return test


def convert_to_recsys_data(df):
    matrix, index_user_id, index_set_id = user_item_matrix(df)
    data_tuples = []
    for i in np.ndindex(matrix.shape):
        if matrix[i] != 0:
            data_tuples.append((matrix[i], index_user_id[i[0]], index_set_id[i[1]]))
    data = Data()
    data.set(data_tuples)
    return data



class Recommender:
    def __init__(self, data, k=10):
        self.svd = SVD()
        self.svd.set_data(data)
        self.svd.compute(k=k, min_values=2)

    def evaluate(self, test):
        user_ids = list(set([i[1] for i in test]))
        user_purchases = {i: [] for i in user_ids}

        for order in test:
            user_purchases[order[1]].append(order[2])

        for user_id in user_ids:
            user_purchases[user_id] = list(set(user_purchases[user_id]))

        average_prec = []
        for user_id in user_ids:
            recommendation = self.svd.recommend(user_id, is_row=True, n=10)
            rec_set_ids = [i[0] for i in recommendation]
            true_set_ids = user_purchases[user_id]
            avg_prec = apk(true_set_ids, rec_set_ids)
            average_prec.append(avg_prec)

        return sum(average_prec) / len(user_ids)

train, test = train_test(read_data(), test_size=0.1)
test = filter_new(train, test)

train = convert_to_recsys_data(train)
test = convert_to_recsys_data(test)

rec = Recommender(train)
